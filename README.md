# Header Field For django Forms  

This project is to allow to insert only text fields in a Django form, which can be used as header section, and have no user input.

There are three files invoved tah you may include in your app 


** widgets.py : Contains the new widget class , which render acustom template with no input field
** customfields.py : Contains the customfield which will have no validators  
** header-form-field.html : To render the field.  

### widgets.py 

```
from django.forms.widgets import Widget
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string


class HeaderWidget(Widget):
    def __init__(self, attrs=None, label=None, tag='h1'):
        self.label = label
        self.tag = tag
        super(HeaderWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):

        widget = render_to_string("blocks/header-form-field.html", {
            'tag'   : self.tag,
            'label' : self.label,
            })
        return widget

```

### customfields.py  

```
from django import forms
from django.forms.fields import Field
from . import widgets

class HeaderField(Field):
    widget = widgets.HeaderWidget
    # This is to avoid validation
    validators = []
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def clean(self, value=''):
        """
        It seem the clean method in mandatory when creating a Field class.
        Jaust return value without validation
        """
        return value

```

### header-form-field.html  

`<{{ tag }} class="your-custom-class">{{ label }}</{{ tag }}>`


## Usage  

In your form class you can include a field wich will be Just a heading for a section .
Eg:

```
class MyForm(GenericForm):

 header1   = customfileds.HeaderField(label=_(""),required=False,label_suffix='', 
                                        widget=widgets.HeaderWidget(label=_("My custom Label text here"),tag="h3"))
 field 1   = whatever input field you have...
```

### Explanation:  

label=_("") -> we live it empty so we use our custon label with custom tag (It could be filled, and we would have double headings)

 widget=widgets.HeaderWidget( --> allow us to pass params to the widget.  
 We can use 2:  
    label= Our custom text for the section header  
    tag =  a custom html tag (h1, h2, span ....)  
    
    
    
    
 